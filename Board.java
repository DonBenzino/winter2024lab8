public class Board{
	
	private Tile[][] grid;
	private final int size;
	private final String BLUE = "\u001B[34m";
	private final String YELLOW = "\u001B[33m";
	private final String WHITE = "\u001B[37m";
	
	
	//Contructor
	public Board(){
		this.size = 3;
		this.grid = new Tile[this.size][this.size];
		
		//Nested Loop to set tiles to _
		for(int i = 0; i<this.size;i++){
			for(int y = 0; y<this.size;y++){
				this.grid[i][y] = Tile.BLANK;
			}
		}
	}
	
	//toString
	public String toString(){
		String result = "0 " + BLUE + "1 2 3 \n" + WHITE;
		for(int i = 0; i<this.size;i++){
			result += YELLOW + (i+1) + " " + WHITE;
			for(int y = 0; y<this.size;y++){
				result += this.grid[i][y].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}


//!!!!QUARANtINED CODE!!!!
//FURTHER RESEARCH REQUIRED TO BE ABLE TO USE FOR EACH
/*
	public String toString(){
		String result = "";
		for(Tile[] row:this.grid){
			for(Tile tile:row){
				result += tile.getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
*/
	//Valid placement checker and placement method
	public boolean placeToken(int row, int col, Tile playerToken){
		if(row > this.size || row < 1 || col >this.size || col < 1 || this.grid[row-1][col-1] != Tile.BLANK){
			return false;
		}
		this.grid[row-1][col-1] = playerToken;
		return true;
	}
	
	//Tie checker
	public boolean checkFull(){
		for(Tile[] row:this.grid){
			for(Tile tile:row){
				if(tile == Tile.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	//Horizontal win condition checker
	private boolean checkWinningHorizontal(Tile playerToken){
		for(Tile[] row:this.grid){
			int counter = 0;
			for(Tile tile:row){
				if(tile == playerToken){
					counter++;
				}
			}
			if (counter == this.size){
				return true;
			}
		}
		return false;
	}
	
	//Vertical win condition checker
	private boolean checkWinningVertical(Tile playerToken){
		for(int i = 0; i<this.size;i++){
			int counter = 0;
			for(int y = 0; y<this.size;y++){
				if (this.grid[y][i] == playerToken){
					counter++;
				}
			}
			if (counter == this.size){
				return true;
			}
		}
		return false;
	}
	
	//Diagonal win condition checker
	private boolean checkWinningDiagonal(Tile playerToken){
		int counter1 = 0;
		int counter2 = 0;
		//top left to bottom right
		for(int i = 0; i<this.size;i++){
			if (this.grid[i][i] == playerToken){
				counter1++;
			}
		}
		//top right to bottom left
		for(int i = 0; i<this.size;i++){
			if (this.grid[i][this.size - i - 1] == playerToken){
				counter2++;
			}
		}
		
		return  counter1 == this.size || counter2 == this.size;
	}
	
	//Full win checker
	public boolean checkIfWinning(Tile playerToken){
		return checkWinningDiagonal(playerToken) || checkWinningHorizontal(playerToken) || checkWinningVertical(playerToken);
	}
	
}