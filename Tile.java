public enum Tile{
	BLANK("_"),
	X("X"),
	O("O");
	
	private final String name;
	
	private Tile(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
}