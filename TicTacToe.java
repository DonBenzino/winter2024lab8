import java.util.Scanner;
public class TicTacToe{
	public static void main(String[] args){
		//Colours
		final String BLUE = "\u001B[34m";
		final String YELLOW = "\u001B[33m";
		final String WHITE = "\u001B[37m";
		
		//Variables
		System.out.println("Hello players");
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		Scanner reader = new Scanner(System.in);
		
		//Main Loop
		while(!(gameOver)){
			int row = 0;
			int col = 0;
			System.out.println(board);
			System.out.println(YELLOW + "In which row will you place your token" + WHITE);
			row = Integer.parseInt(reader.nextLine());
			System.out.println(BLUE + "In which column will you place your token" + WHITE);
			col = Integer.parseInt(reader.nextLine());
			
			boolean placed = board.placeToken(row, col, playerToken);
			//Secondary loop (if placement invalid)
			while(!placed){
				System.out.println("Invalid placement, try again");
				System.out.println("In which row will you place your token");
				row = Integer.parseInt(reader.nextLine());
				System.out.println("In which column will you place your token");
				col = Integer.parseInt(reader.nextLine());
				placed = board.placeToken(row, col, playerToken);
			}
			//Win, tie or continue check
			if(board.checkIfWinning(playerToken)){
				break;
			}else if(board.checkFull()){
				gameOver = true;
			}else{
				if (player == 1){
					player = 2;
					playerToken = Tile.O;
				}else{
					player = 1;
					playerToken = Tile.X;
				}
			}
			System.out.println("\033[H\033[2J");
		}
		//Win or loss declaration
		if(gameOver){
			System.out.println("tie");
			System.out.println(board);
		}else{
			System.out.println("player " + player + " wins");
			System.out.println(board);
		}
	}
}